def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([
        usernamePassword(credentialsId: 'dockerhub-xstodu07',
                         passwordVariable: 'PASS',
                         usernameVariable: 'USER')
    ]) {
        env.DOCKER_IMAGE_NAME = "$USER/private:java-maven-app-${params.VERSION_CHOICE}"
        sh('docker build -t $DOCKER_IMAGE_NAME .')
        sh('echo $PASS | docker login -u $USER --password-stdin')
        sh('docker push $DOCKER_IMAGE_NAME')
    }
}

def deployApp() {
    echo 'deploying the application...'
} 

return this
