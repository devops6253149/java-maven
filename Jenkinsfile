def gv

pipeline {
    agent any
    parameters {
        string(
            name: 'VERSION',
            defaultValue: 'latest',
            description: 'Version to deploy on prod')
        text(
            name: 'APP_CONF',
            defaultValue: '[DEFAULT]\n' +
                'log_dir = /var/log/app\n' +
                'debug = False\n',
            description: '')
        choice(
            name: 'VERSION_CHOICE',
            choices: ['1.2', '1.3'],
            description: '')
        booleanParam(
            name: 'EXECUTE_TESTS',
            defaultValue: 'True',
            description: '')
    }
    tools { 
        maven 'maven-3.9'
    }
    environment {
        NEW_VERSION = '0.0.1'
    }
    stages {
        stage("init") {
            steps {
                script {
                        gv = load "script.groovy"
                        echo "building jar $NEW_VERSION"
                        echo "buildtag: ${BUILD_TAG}"
                        echo "APP_CONF: ${params.APP_CONF}"
                        echo "VERSION_CHOICE: ${params.VERSION_CHOICE}"
                        echo "VERSION: ${params.VERSION}"
                }
            }
        }
        stage("test") {
            when {
                expression {
                    params.EXECUTE_TESTS
                }
            }
            steps {
                script {
                    gv.buildJar()
                }
            }
        }
        stage("build") {
            when {
                expression {
                    BRANCH_NAME == 'main'
                }
            }
            steps {
                script {
                    gv.buildImage()
                }
            }
        }
        stage("deploy") {
            when {
                expression {
                    BRANCH_NAME == 'main'
                }
            }
            steps {
                script {
                    gv.deployApp()
                }
            }
        }
    }   
}
